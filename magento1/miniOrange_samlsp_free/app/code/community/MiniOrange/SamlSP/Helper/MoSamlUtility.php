<?php
/** miniOrange enables user to log in through mobile authentication as an additional layer of security over password.
    Copyright (C) 2015  miniOrange

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
* @package 		miniOrange OAuth
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
*/
/**
This library is miniOrange Authentication Service. 
Contains Request Calls to Customer service.
**/
class MiniOrange_SamlSP_Helper_MoSamlUtility extends Mage_Core_Helper_Abstract{
	
	private $email;
	private $phone;
	private $hostname = "https://auth.miniorange.com";
	private $pluginName = 'Magento SAML 2.0 SP Plugin';
	private $defaultCustomerKey = "16555";
	private $defaultApiKey = "fFd2XcvTGDemZvbw1bcUesNJWEqKbbUq";
	
	function check_customer($email){
		$url 	= $this->hostname . '/moas/rest/customer/check-if-exists';
		$fields = array(
			'email' 	=> $email,
		);
		$json = json_encode( $fields );
		//$authHeader 	= $this->createAuthHeader($customerKey,$apiKey);
		$response 		= $this->callAPI($url, $json);
		return $response;
	}
	
	
	function send_otp_token($email, $phone, $sendToEmail = TRUE, $sendToPhone = FALSE){
		$url = $this->hostname . '/moas/api/auth/challenge';
		$ch = curl_init ( $url );
		$customerKey = $this->defaultCustomerKey;
		$apiKey = $this->defaultApiKey;
		if ($sendToEmail) {
			$fields = array (
					'customerKey' => $customerKey,
					'email' => $email,
					'authType' => 'EMAIL',
					'transactionName' => $this->pluginName
			);
		} else {
			$fields = array (
					'customerKey' => $customerKey,
					'phone' => $phone,
					'authType' => 'SMS',
					'transactionName' => $this->pluginName
			);
		}
		$json = json_encode ( $fields );
		$authHeader 	= $this->createAuthHeader($customerKey,$apiKey);
		$response 		= $this->callAPI($url, $json, $authHeader);
		return $response;
	}
	
	
	function validate_otp_token($transactionId,$otpToken){
		$url = $this->hostname . '/moas/api/auth/validate';
		$customerKey = $this->defaultCustomerKey;
		$apiKey = $this->defaultApiKey;		
		$fields = '';
		// *check for otp over sms/email
		$fields = array (
				'txId' => $transactionId,
				'token' => $otpToken 
		);
		$json = json_encode ( $fields );
		$authHeader 	= $this->createAuthHeader($customerKey,$apiKey);
		$response 		= $this->callAPI($url, $json, $authHeader);
		return $response;
	}
	
	function create_customer($email,$phone,$password){
		$url = $this->hostname . '/moas/rest/customer/add';		
		$fields = array(
			'companyName' => $_SERVER['SERVER_NAME'],
			'areaOfInterest' => $this->pluginName,
			'email' => $email,
			'phone' => $phone,
			'password' => $password
		);
		$json = json_encode($fields);
		//$authHeader 	= $this->createAuthHeader($customerKey,$apiKey);
		$response 		= $this->callAPI($url, $json);
		return $response;
	}
	
	function get_customer_key($email,$password) {
		$url = $this->hostname .  "/moas/rest/customer/key";
		$fields = array(
			'email' => $email,
			'password' => $password
		);
		$json = json_encode($fields);
		//$authHeader 	= $this->createAuthHeader($customerKey,$apiKey);
		$response 		= $this->callAPI($url, $json);
		return $response;
	}
	
	
	function submit_contact_us( $q_email, $q_phone, $query, $user) {
		$url = $this->hostname .  "/moas/rest/customer/contact-us";
		$query = '[Magento SAML SP Plugin]: ' . $query;
		$fields = array(
			'firstName'			=> $user->getFirstname(),
			'lastName'	 		=> $user->getLastname(),
			'company' 			=> $_SERVER['SERVER_NAME'],
			'email' 			=> $q_email,
			'phone'				=> $q_phone,
			'query'				=> $query
		);
		$json = json_encode( $fields );
		//$authHeader 	= $this->createAuthHeader($customerKey,$apiKey);
		$response 		= $this->callAPI($url, $json);
		return $response;
	}
	
	
	function forgot_password($email,$customerKey,$apiKey){
		$url = $this->hostname . '/moas/rest/customer/password-reset';
		//*check for otp over sms/email
		$fields = array(
			'email' => $email
		);
		$json = json_encode($fields);
		$authHeader 	= $this->createAuthHeader($customerKey,$apiKey);
		$response 		= $this->callAPI($url, $json, $authHeader);
		return $response;
	}
	
	function mo_check_user_already_exist($email,$customerKey,$apiKey){
		
		$url = $this->hostname . '/moas/api/admin/users/search';
		$fields = array(
			'username' => $email
		);
		$json = json_encode($fields);
		$authHeader 	= $this->createAuthHeader($customerKey,$apiKey);
		$response 		= $this->callAPI($url, $json, $authHeader);
		return $response;
	}
	

	function check_customer_valid($customerKey,$apiKey){
		$url = $this->hostname . '/moas/rest/customer/license';
		$fields = array(
					   'customerId' => $customerKey,
					   'applicationName' => $this->$pluginName
				);
		$json = json_encode($fields);
		$authHeader 	= $this->createAuthHeader($customerKey,$apiKey);
		$response 		= $this->callAPI($url, $json, $authHeader);
		return $response;
	}

	private function createAuthHeader($customerKey, $apiKey)
	{
		$currentTimestampInMillis = round(microtime(true) * 1000);
		$currentTimestampInMillis = number_format($currentTimestampInMillis, 0, '', '');
		$stringToHash = $customerKey . $currentTimestampInMillis . $apiKey;
		$authHeader = hash("sha512", $stringToHash);
		$header = array (
				"Content-Type: application/json",
				"Customer-Key: $customerKey",
				"Timestamp: $currentTimestampInMillis",
				"Authorization: $authHeader"
		);
		return $header;
	}
		
	private function callAPI($url, $json_string, $headers = array("Content-Type: application/json"))
	{
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 		
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		if(!is_null($headers)) curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POST, true);
		if(!is_null($json_string)) curl_setopt($ch, CURLOPT_POSTFIELDS, $json_string);
		$content = curl_exec($ch);
	
		if (curl_errno($ch)) {
			echo 'Request Error:' . curl_error($ch);
			exit();
		}
	
		curl_close($ch);
		return $content;
	}
	
}?>