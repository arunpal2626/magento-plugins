<?php

class MiniOrange_Api_Block_MoApi extends Mage_Core_Block_Template
{
    const CUSTOMER_MODEL    = "customer_model";
    const MO_UTILITY        = "mo_utility" ;
    const ADMIN_SESSION     = "admin_session";
    const CORE_SESSION      = "admin_session";
    const ADMIN_HELPER      = "admin_helper";
    const MESSAGE_HANDLER   = "message_handler";
    const JSON_HANDLER      = "json_handler";
    const ENDPOINT_DATA     = [
                                    "endpoints" => [
                                        "search"  => [
                                            "method"        => "GET",
                                            "endpoint"      => "customer/api/search",
                                            "description"   => "Search a customer based on filter criteria"
                                        ],
                                        "authenticate" => [
                                            "method"        => "POST",
                                            "endpoint"      => "customer/api/authenticate",
                                            "description"   => "Validate a customer based on username and password passed"
                                        ]
                                    ]
                                ];


    /**
     * @param $type
     * @return false|Mage_Core_Helper_Abstract|Mage_Customer_Model_Customer|Mage_Core_Model_Abstract|Mage_Adminhtml_Helper_Data|Mage_Core_Model_Session|MiniOrange_Api_Helper_Messages
     * @throws Mage_Core_Exception
     */
    public static function _fetch($type)
    {
        switch($type)
        {
            case self::CUSTOMER_MODEL:
                $model = Mage::getModel("customer/customer");
                $model->setWebsiteId(Mage::app()->getWebsite()->getId());
                return $model;
            case self::MO_UTILITY:
                return Mage::helper("MiniOrange_Api/MoApi");
            case self::ADMIN_SESSION:
                return Mage::getSingleton('admin/session');
            case self::CORE_SESSION:
                return Mage::getSingleton('core/session');
            case self::ADMIN_HELPER:
                return Mage::helper('adminhtml');
            case self::MESSAGE_HANDLER:
                return Mage::helper('MiniOrange_Api/Messages');
            case self::JSON_HANDLER:
                return Mage::helper('MiniOrange_Api/JsonData');
        }
        return null;
    }


    public static function _getEndpointsData()
    {
        //TODO: Add request / response examples
        return json_encode(self::ENDPOINT_DATA);
    }


    public static function is_curl_installed()
    {
        return in_array('curl', get_loaded_extensions());
    }


    /**
     * @param $value
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getExtensionPageUrl($value)
    {
        return self::_fetch(self::ADMIN_HELPER)->getUrl($value,array('_secure'=>true));
    }


    /**
     * @return string
     * @throws Mage_Core_Exception
     */
    public function getFormKeyHTML()
    {
        return sprintf(
            '<input type="hidden" name="form_key" value="%s" />',
            self::_fetch(self::CORE_SESSION)->getFormKey()
        );
    }

    public function isCustomerRegistered()
    {
        $email = MiniOrange_Api_Helper_Data::getConfig(MiniOrange_Api_Helper_Data::CUSTOMER_EMAIL);
        $key = MiniOrange_Api_Helper_Data::getConfig(MiniOrange_Api_Helper_Data::CUSTOMER_KEY);

        return !empty($email) && !empty($key);
    }

    public function getCustomerData()
    {
        return [
            'email' => MiniOrange_Api_Helper_Data::getConfig(MiniOrange_Api_Helper_Data::CUSTOMER_EMAIL),
            'key'   => MiniOrange_Api_Helper_Data::getConfig(MiniOrange_Api_Helper_Data::CUSTOMER_KEY),
            'apiKey'=> MiniOrange_Api_Helper_Data::getConfig(MiniOrange_Api_Helper_Data::CUSTOMER_API_KEY),
            'token' => MiniOrange_Api_Helper_Data::getConfig(MiniOrange_Api_Helper_Data::CUSTOMER_TOKEN)
        ];
    }


    /**
     * @return Mage_Core_Model_Message_Collection
     * @throws Mage_Core_Exception
     */
    public function getMessage()
    {
        return self::_fetch(self::MESSAGE_HANDLER)->getMessage();
    }
}