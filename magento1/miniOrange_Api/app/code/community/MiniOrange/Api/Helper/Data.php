<?php

class MiniOrange_Api_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CUSTOMER_EMAIL        = 'miniOrange/api/customerEmail';
    const CUSTOMER_KEY          = 'miniOrange/api/customerKey';
    const CUSTOMER_API_KEY      = 'miniOrange/api/apiKey';
    const CUSTOMER_TOKEN        = 'miniOrange/api/token';
    const REG_STATUS            = 'miniOrange/api/regstatus';
    const TXT_ID                = 'miniOrange/api/txid';

    const HOSTNAME            	= "https://auth.miniorange.com";
    const DEFAULT_CUSTOMER_KEY	= "16555";
    const DEFAULT_API_KEY 	= "fFd2XcvTGDemZvbw1bcUesNJWEqKbbUq";
    const STATUS_COMPLETE_LOGIN = "reg_complete";
    const VERIFICATION_REQUIRED = "verification_required";


    /* Function to extract config stored in the database */
    public static function getConfig($config)
    {
        switch ($config)
        {
            case self::CUSTOMER_EMAIL :
                return Mage::getStoreConfig(self::CUSTOMER_EMAIL);
            case self::CUSTOMER_KEY :
                return Mage::getStoreConfig(self::CUSTOMER_KEY);
            case self::CUSTOMER_API_KEY :
                return Mage::getStoreConfig(self::CUSTOMER_API_KEY);
            case self::CUSTOMER_TOKEN :
                return Mage::getStoreConfig(self::CUSTOMER_TOKEN);
            default:
                return NULL;
        }
    }


    public static function storeConfig($key,$value)
    {
        $storeConfig = new Mage_Core_Model_Config();
        $storeConfig ->saveConfig($key, $value, 'default', 0);
    }


    public static function storeConfigs(array $keyValuePairs)
    {
        $storeConfig = new Mage_Core_Model_Config();
        foreach ($keyValuePairs as $key => $value) {
            $storeConfig ->saveConfig($key, $value, 'default', 0);
        }
    }

}  
