<?php

class MiniOrange_Api_Helper_Messages extends Mage_Core_Helper_Abstract
{
    const SUCCESS   = 'SUCCESS';
    const ERROR     = 'ERROR';
    const NOTICE    = 'NOTICE';


    public function displayMessage($message, $type)
    {
        $coreSession = MiniOrange_Api_Block_MoApi::_fetch(
            MiniOrange_Api_Block_MoApi::CORE_SESSION
        );
        $coreSession->getMessages( true );
        switch ($type)
        {
            case self::SUCCESS:
                $coreSession->addSuccess($message );          break;
            case self::ERROR:
                $coreSession->addError($message);             break;
            case self::NOTICE:
                $coreSession->addNotice($message);            break;
            default:
                $coreSession->addWarning($message);           break;
        }
    }

    public function getMessage()
    {
        $coreSession = MiniOrange_Api_Block_MoApi::_fetch(
            MiniOrange_Api_Block_MoApi::CORE_SESSION
        );
        $err = $coreSession->getMessages()->getItemsByType("error")[0];
        $success = $coreSession->getMessages()->getItemsByType("success")[0];
        $notice = $coreSession->getMessages()->getItemsByType("notice")[0];
        $warning = $coreSession->getMessages()->getItemsByType("warning")[0];
        $message = [];

        if(!MiniOrange_Api_Helper_MoApi::isBlank($err))
             $message = [ 'error' => $err->getCode() ];
        elseif(!MiniOrange_Api_Helper_MoApi::isBlank($success))
            $message = [ 'success' => $success->getCode() ];
        elseif(!MiniOrange_Api_Helper_MoApi::isBlank($notice))
            $message = [ 'notice' => $message->getCode() ];
        elseif(!MiniOrange_Api_Helper_MoApi::isBlank($warning))
            $message = [ 'warning' => $warning->getCode() ];

        return $message;
    }
}