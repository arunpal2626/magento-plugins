<?php

class MiniOrange_Api_Customer_ApiController extends Mage_Core_Controller_Front_Action
{
    /** @var Mage_Customer_Model_Customer */
    private $customerModel;
    /** @var MiniOrange_Api_Helper_JsonData */
    private $moUtility;

    /** MiniOrange_Api_Customer_ApiController constructor.
     * @throws Mage_Core_Exception
     */
    protected function _construct()
    {
        parent::_construct();
        $this->customerModel = MiniOrange_Api_Block_MoApi::_fetch(
            MiniOrange_Api_Block_MoApi::CUSTOMER_MODEL
        );
        $this->moUtility = MiniOrange_Api_Block_MoApi::_fetch(
            MiniOrange_Api_Block_MoApi::JSON_HANDLER
        );
    }


    /**
     * @throws Zend_Controller_Response_Exception
     */
    public function indexAction()
    {
        $this->moUtility->sendJSONData(
            200,
            MiniOrange_Api_Block_MoApi::_getEndpointsData(),
            $this->getResponse()
        )   ;
    }


    /**
     * @throws Zend_Controller_Response_Exception
     */
    public function errorAction()
    {
        $this->moUtility->sendJSONData(
            400,
            $this->moUtility->generateJSONData("Bad Request", MiniOrange_Api_Helper_JsonData::ERROR),
            $this->getResponse()
        );
    }

    /**
     * @throws Zend_Controller_Response_Exception
     */
    public function searchAction()
    {
        try {

            $params = $this->getRequest()->getQuery();
            $searchFilters = $params['searchCriteria'][0];
            $customer = null;
            if ($searchFilters['field'] === "email") {
                $customer = $this->customerModel->loadByEmail($searchFilters['value']);
            }
            //var_dump($customer->getData());exit;
            $jsonData = $customer!= NULL && $customer->getId() != NULL
                ? $this->moUtility->generateJSONData(
                    "Customer exists",
                    MiniOrange_Api_Helper_JsonData::SUCCESS,
                    ["customer" => $customer->getData()]
                )
                : $this->moUtility->generateJSONData("Customer doesn't exist", MiniOrange_Api_Helper_JsonData::ERROR);

            $statusCode = $customer!= NULL && $customer->getId() != NULL ? 200 : 404;
            $this->moUtility->sendJSONData($statusCode, $jsonData, $this->getResponse());

        }catch (\Exception $e) {
            $this->moUtility->sendJSONData(
                500,
                $this->moUtility->generateJSONData("Unknown Error:".$e->getMessage(), MiniOrange_Api_Helper_JsonData::ERROR),
                $this->getResponse()
            );
        }
    }

    /**
     * @throws Zend_Controller_Response_Exception
     */
    public function authenticateAction()
    {
        try {
                $params = $this->getRequest()->getRawBody();
                $params = json_decode($params,TRUE);

                if(!array_key_exists('username',$params) || !array_key_exists('password',$params)) {
                    $this->moUtility->sendJSONData(
                        400,
                        $this->moUtility->generateJSONData("Bad Request",MiniOrange_Api_Helper_JsonData::ERROR),
                        $this->getResponse()
                    );
                }

               $authenticated = $this->customerModel->authenticate($params['username'],$params['password']);

                $this->moUtility->sendJSONData(
                    $authenticated ? 200 : 401,                       // status code
                    $authenticated
                        ? $this->moUtility->generateJSONData(                   // success json data
                            "Authenticated Successfully",
                            MiniOrange_Api_Helper_JsonData::SUCCESS
                        )
                        : $this->moUtility->generateJSONData(                   // error json data
                            "Invalid Username or Password",
                            MiniOrange_Api_Helper_JsonData::ERROR
                        ),
                    $this->getResponse()                              // response data
                );

        }catch (\Exception $e) {
            $this->moUtility->sendJSONData(
                500,
                $this->moUtility->generateJSONData("Unknown Error : ".$e->getMessage(), MiniOrange_Api_Helper_JsonData::ERROR),
                $this->getResponse()
            );
        }
    }
}


