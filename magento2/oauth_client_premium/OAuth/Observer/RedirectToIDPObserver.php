<?php 

namespace MiniOrange\OAuth\Observer;

use Magento\Framework\Event\ObserverInterface;
use MiniOrange\OAuth\Helper\Exception\SAMLResponseException;
use MiniOrange\OAuth\Helper\Exception\InvalidSignatureInResponseException;
use MiniOrange\OAuth\Helper\OAuthMessages;
use Magento\Framework\Event\Observer;
use MiniOrange\OAuth\Controller\Actions\ReadAuthorizationResponse;
use MiniOrange\OAuth\Helper\OAuthConstants;

class RedirectToIDPObserver implements ObserverInterface{
    
    private $requestParams = array (
		'option'
    );
    
    private $controllerActionPair = array (
		'account' => array('login','create'),
		'auth' => array('login'),
    );
    
    private $messageManager;
	private $logger;
	private $readAuthorizationResponse;
	private $oauthUtility;
	private $adminLoginAction;
	private $testAction;

	private $currentControllerName;
	private $currentActionName;
	private $requestInterface;
	private $request;

	public function __construct(\Magento\Framework\Message\ManagerInterface $messageManager,
								\Psr\Log\LoggerInterface $logger,
								\MiniOrange\OAuth\Controller\Actions\ReadAuthorizationResponse $readAuthorizationResponse,
								\MiniOrange\OAuth\Helper\OAuthUtility $oauthUtility,
								\MiniOrange\OAuth\Controller\Actions\AdminLoginAction $adminLoginAction,
								\Magento\Framework\App\Request\Http $httpRequest,
								\Magento\Framework\App\RequestInterface $request,
								\MiniOrange\OAuth\Controller\Actions\ShowTestResultsAction $testAction)
    {
		//You can use dependency injection to get any class this observer may need.
		$this->messageManager = $messageManager;
		$this->logger = $logger;
		$this->readAuthorizationResponse = $readAuthorizationResponse;
		$this->$oauthUtility = $$oauthUtility;
		$this->adminLoginAction = $adminLoginAction;
		$this->currentControllerName = $httpRequest->getControllerName();
		$this->currentActionName = $httpRequest->getActionName();
		$this->request = $request;
		$this->testAction = $testAction;
		
    }

    	/**
	 * This function is called as soon as the observer class is initialized.
	 * Checks if the request parameter has any of the configured request 
	 * parameters and handles any exception that the system might throw.
	 * 
	 * @param $observer
	 */
    public function execute(Observer $observer)
    {		
		$keys 			= array_keys($this->request->getParams());
		$operation 		= array_intersect($keys,$this->requestParams);
		try{
			if($this->checkIfUserShouldBeRedirected())
			{	//redirecting to the loginrequest controller
				
				$observer->getControllerAction()->getResponse()
						 ->setRedirect($this->oauthUtility->getSPInitiatedUrl());
			}
		}catch (\Exception $e){
			// if($isTest) { // show a failed validation screen
			// 	$this->testAction->setSamlException($e)->setHasExceptionOccurred(TRUE)->execute();
			// }
			$this->messageManager->addErrorMessage($e->getMessage());
			$this->logger->debug($e->getMessage());
		}
	}

    	/**
	 * This function checks if user needs to be redirected to the
	 * registered IDP with AUthnRequest. First check if admin has 
	 * enabled autoRedirect. Then check if user is landing on one of the
	 * admin or customer login pages. If both of those are true
	 * then return TRUE other return FALSE.
	 */

    private function checkIfUserShouldBeRedirected()
    {
        // return false if auto redirect is not enabled
        if($this->oauthUtility->getStoreConfig(OAuthConstants::AUTO_REDIRECT)!="1"
            || $this->oauthUtility->isUserLoggedIn()) return FALSE;
        // check if backdoor is enabled and samlsso=false
        if($this->oauthUtility->getStoreConfig(OAuthConstants::BACKDOOR)=="1"
            && array_key_exists(OAuthConstants::OAuth_SSO_FALSE,$this->request->getParams())) return FALSE;
        // now check if user is landing on one of the login pages
        $action = array_key_exists($this->currentControllerName,$this->controllerActionPair)
            ? $this->controllerActionPair[$this->currentControllerName] : NULL;
        return !is_null($action) && is_array($action) ? in_array($this->currentActionName,$action) : FALSE;
    }

}